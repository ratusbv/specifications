# Ratus Specifications

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

-----------------------------------------------------------------------

This repository contains specifications and standards drafted by
Ratus B.V. to enhance the quality of it's own produce, including, but
not limited to, code, documents, workflows and products.

Third parties are allowed to apply any of these specifications based on
their own requirements.

-----------------------------------------------------------------------

### Table of contents

- [SPEC0000](spec/0000.txt) Specification Format
- [SPEC0001](spec/0001.txt) Javascript Styling
- [SPEC0002](spec/0002.pdf) Ratus Unilicense
